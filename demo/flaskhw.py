from flask import Flask

app = Flask("HelloFlask")

@app.route("/")
def hello():
    return "Hello, Flask!"

@app.route("/faraway")
def faraway():
    return r'<a href="/">BACK</a>'

if __name__ == "__main__":
    app.run()