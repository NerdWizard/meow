# 初章

## 零

Python下构建一个Web服务的[最简单方案][cs_ss]是这样的：

```shell
$ python -m SimpleHTTPServer
```

此时，一个静态文件服务器已经成功部署了。

> 这是一个堪比import antigravity的功能←_←

> 我会尽力把难度曲线控制在指数线之下的，不过毕竟篇(all)幅(hackers)有(are)限(lazy)

## 一

除了`SimpleHTTPServer`这个黑魔法，[Python 3标准库手册的第21章][pl_21]中还列举了许多随Python默认安装的用于Web开发的库程序。
这些库向Python用户提供了简单但完备的工具集，保证了Python用于Web开发的**可行**，而混战厮杀中的各路大小框架则在此基础上提供了更高阶的抽象，使开发者得以将更多精力集中在**更重要的业务逻辑**上，保证了Python Web的**易用**。

在[Flask的官网](http://flask.pocoo.org)首页上，有一个与下面的例子类似的Hello World程序

```python
from flask import Flask

app = Flask("HelloFlask")

@app.route("/")
def hello():
    return "Hello, Flask!"

if __name__ == "__main__":
    app.run()
```

最核心的三行只交代了两件事情：

1. 由`hello`函数处理对根的访问
2. `hello`函数只是简单的返回`"Hello, Flask!"`

运行这段代码，访问`localhost:5000`时就会看到一行简单的`Hello, Flask!`。

`HelloFlask`应用的唯一功能就是对根目录的访问返回一句话。上面的代码也只交代了这件知情，与网络有关的一切都悉数交由`Flask`代管。这就是框架的意义。

向例子中加入

```python
@app.route("/faraway")
def faraway():
    return r'<a href="/">BACK</a>'
```

可以为`HelloFlask`添加一个新功能，访问`localhost:5000/faraway`时提供一个返回主页的链接。

至此，我们可以对Flask最基本的使用作出如下的感性总结：

1. 创建一个App(Flask实例)
2. 使用装饰器把对特定地址的访问绑定到一个函数上
3. 负责响应请求的函数返回的字符串被视作HTML返回给用户

看上去并不复杂嘛！

本文剩下的篇幅主要用于讨论如何从WSGI开始，逐步自底向上地向构建一个微型Web框架，并在新框架的基础上构建一个示范性的小应用，以此示Python Web栈上各方面的基本技巧。

引言（终于）完。

## 二

本着临(tou)摹(lan)的初衷，我小心谨慎的clone了Flask的源码，发现Flask在底层使用了[Werkzeug][wz_id]，而且Werkzeug的官网上的`A Strong Foundation`一节如是说

> Werkzeug is the base of frameworks such as Flask and more,
> as well as in house frameworks developed for commercial products and websites.

简单翻阅其Quick Start，发现实现一个与Flask版本类似的`HelloWSGI`，其全部代码不过如此

```python
from werkzeug.wrappers import Response
from werkzeug.serving import run_simple

def app(environ, start_response):
    res = Response('Hello, Web')
    return res(environ, start_response)

if __name__ == '__main__':
    run_simple('localhost', 5000, app)
```                    

只是去掉了路由嘛...现在访问`localhost:5000`下的所有路径都会得到一句`Hello, Web`了。

不过看上去并不坏的样子，接下来让我们做一个框架玩一下好了。

后续章节：[Werkzeug之上](wrappers-on-wrappers)

[cs_ss]: http://coolshell.cn/articles/1480.html
[pl_21]: https://docs.python.org/3/library/internet.html
[wz_id]: http://werkzeug.pocoo.org
