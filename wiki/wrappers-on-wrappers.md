# Werkzeug之上

## 完美假设

为了进一步的简化问题，我们对Meow做出以下假设：

+ 仅支持Python3.4
+ 默认UTF-8编码
+ 仅支持PUSH和GET操作
+ 非必须的功能一律使用第三方库

同时，为了控制篇幅同时保证行文流畅，我也假设你听说过下面这些东西：

+ 高阶函数
+ Python - 装饰器
+ Python - 类
+ Python - MagicMethods（魔术方法）
+ Werkzeug - 最基础的使用

并不需要知道多少细节，感性认识就好。

## Step 1

把上一个例子用更**面向对象**的风格重写一下，可以得到这样一个东西

```python
from werkzeug.wrappers import Response, Request
from werkzeug.serving import run_simple

class Meow:
    def __call__(self, env, fn):
        return Response('Meow~')(env, fn)

def run(app, host='localhost', port=5000, debug=False):
    run_simple(host, port, app, use_debugger=debug)

if __name__ == '__main__':
    run(Meow())
```

除了多了两行`run`函数的定义，似乎也没什么不对...

> `__call__`这种东西就是Python中类的魔术方法之一，详询Google

咳咳...

接下来还是先解决路由的问题吧。

> Werkzeug是有完整的解决方案的，不过什么都用别人的还要我作甚[托腮]

> 接下来会用到装饰器和一点黑魔法（高阶函数），不熟悉的话请先科普

要完成这件事情大体上分为两步，第一步是找到一种更自然地表示方法，来包装一个接受参数、返回字符串的函数，就像Flask那样；第二步是设法把URL绑定到函数上。

第一步并不难，轻微改造一下`__call__`方法就可以了：用一个字典记录路径-函数的关系，在`__call__`的时候检查字典就可以了。第二步需要利用装饰器的本质，即

```python
@magic
def foo():
    return 42
```

和

```python
foo = magic(foo)
```

这两段代码是等价的。换句话说，所谓的装饰器只是一个语法糖，而装饰器本身只是一个**接受一个函数作为参数，并返回一个新函数的函数**，也就是所谓的高阶函数。
Python中有一个B格极高的词叫做**面向截面的程序设计**，指的就是对装饰器的灵活运用。

同时，充分利用**函数也是对象**这一特性，还可以进一步构造所谓的“接收参数的装饰器”。

```python
def add(n):
    def _add_n(fn):
        def wrapped(*args, **kwargs):
            return n + fn(*args, **kwargs)
        return wrapped
    return _add_n

@add(9)
def ans():
    return 31

ans() # => 42
```
也就是先返回装饰器、再返回新函数。关于装饰器，这就是最重要的一切。

利用这些特性，可以很轻松的利用装饰器完成路由绑定：

```python
class Meow(object):
    def __init__(self):
        self.route_record = dict()

    def __call__(self, env, fn):
        path = Request(env).path
        callback = self.route_record.get(path, None)
        if callback is not None:
            return Response(callback())(env, fn)
        else:
            raise 404

    def bind(self, url):
        def binder(fn):
            self.route_record[url] = fn
            return fn
        return binder
```

接下来很自然地重造上面的`HelloWorld`

```python
from meow import Meow, run

app = Meow()

@app.bind('/')
def index():
    return "Hello, Meow~"

@app.bind('/about')
def about():
    return "This is my first web framework"

if __name__ == '__main__':
    run(app)
```

That's all.

后续章节：[参数](arg-paresing)







