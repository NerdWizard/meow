# encoding=utf-8

# step 0: wrappers on wrappers

from werkzeug.wrappers import Response
from werkzeug.serving import run_simple


def app(environ, start_response):
    res = Response('Hello, Web')
    return res(environ, start_response)

if __name__ == '__main__':
    run_simple('localhost', 5000, app)
