# encoding=utf-8

# step1: basic
from werkzeug.wrappers import Response, Request
from werkzeug.serving import run_simple


class Meow(object):
    def __init__(self):
        self.route_record = dict()

    def __call__(self, env, fn):
        path = Request(env).path
        callback = self.route_record.get(path, None)
        if callback is not None:
            return Response(callback())(env, fn)
        else:
            raise 404

    def bind(self, url):
        def binder(fn):
            self.route_record[url] = fn
            return fn
        return binder


def run(app: Meow,
        host: str='localhost',
        port: int=5000,
        debug: bool=False) -> None:
    run_simple(host, port, app, use_debugger=debug)

